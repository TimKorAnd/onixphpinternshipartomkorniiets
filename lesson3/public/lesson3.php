<?php // TODO code style reformat

    declare(strict_types=1);

    namespace lesson3\app;

    use lesson3\app\Helpers\TimeHelper;
    use lesson3\app\Models\products\Processor;
    use lesson3\app\Models\products\Product;
    use lesson3\app\Models\products\Ram;
    use lesson3\app\Models\users\User;

    require '../app/autoload.php';

    date_default_timezone_set('Europe/Uzhgorod');



    echo "\nScript start at: ".date('Y-m-d H:i:s',time()); // TODO kill hardcode

    $userVasiliy = new User('Vasiliy', 100);
    $userBill = new User('Bill Gates',1000000);

    $p = Processor::createProduct([2500,'AMD Ryzer 3', 150, $userVasiliy]);

    $p1 = Processor::createProduct([3000,'AMD Ryzer 5', 250]);
    $p1->setOwner($userVasiliy);

    $p2 = Processor::createProduct([4000,'Intel i9', 2500, null]);
    $p2->setOwner($userBill);

    $r = Ram::createProduct(['DDR4',16,'WD',85]);
    $r1 = Ram::createProduct(['DDR2',4,'Sclerozz', 12, $userVasiliy]);
    $r2 = Ram::createProduct(['DDR5',32, 'Samsung',260]);
    $r2->setOwner($userVasiliy);


    echo "\nIteration over all registered products:";
    $iter = Product::getProductsIterator();
    foreach($iter as $key => $val){
        echo "\n".$key.": ".$val;
    }

    $testUser = new User("testUser",1000);
    //print_r($testUser->listProducts());

    $prod1 = Ram::createProduct(['dimm2',256, 'planka',55]);

    //Error in params test
    $prodError1 = Ram::createProduct([3000,'AMD Ryzer 5', 250, $userVasiliy]);
    $prodError2 = Processor::createProduct(['dimm2',256, 'planka',55, $userBill]);
    var_dump($prodError1);
    var_dump($prodError2);

    $prod = Ram::createProduct(['dimm2',256, 'planka',55, $userBill]);
    $prod = Processor::createProduct([3000,'AMD Ryzer 5', 250, $userVasiliy]);

    try {
        $userVasiliy->sellProduct($userBill, $prod);
        $userVasiliy->sellProduct($userBill, $r1);
        $userVasiliy->sellProduct($userBill, $r1);
        $userVasiliy->sellProduct($testUser, $r2);
    } catch (\Exception $ex){
        echo $ex->getMessage();
    };
    $i = 10;
    while($i--){
        Product::createRandomProduct($testUser);
    }
    echo PHP_EOL;

    foreach($testUser->listProducts() as $product){
        echo ++$i.' '.$product.PHP_EOL;
    };

    echo "\n\nScript completed at: ".date('Y-m-d H:i:s',time());