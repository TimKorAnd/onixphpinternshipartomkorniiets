<?php


    namespace lesson3\app\Models\products;


    use lesson3\app\Models\users\User;

    interface ProductTransaction
    {
        function sellProduct(User $buyer, Product $product): bool;

    }