<?php


    namespace lesson3\app\Models\products;


    use lesson3\app\Models\users\User;

    class Processor extends Product
    {
        private int $frequency;

        /**
         * Processor constructor.
         * @param int $frequency
         * @param string $name
         * @param int $price
         * @param User|null $owner
         */
        private function __construct(int $frequency, string $name, int $price, ?User $owner = null)
        {
            parent::__construct($name, $price, $owner);
            $this->frequency = $frequency;

            parent::$logger->log('alert',
              '{name} {frequency} created',
              ['name' => $name, 'frequency' => $frequency]);

            $this->registration($this);
        }

        /**instantiates instance with private class constructor
         * @param array $params
         * @return static
         */
        public static function createProduct(Array $params) : ?self
        {
            //return new static(...$params);
            try {
                [$frequency, $name, $price, $owner] = $params;
                return new static($frequency, $name, $price, $owner);
            } catch (\Throwable $ex){
                parent::$logger->log('error',
                  'Params for create Processor obj  {params} is incorrect',
                  ['params' => json_encode($params)]);
                return null;
            }
        }

        /**
         * @return int
         */
        public function getFrequency(): int
        {
            return $this->frequency;
        }



        public function __toString(): string
        {
            $owner = $this->getOwner() ? $this->getOwner()->getUsername() : 'no owner yet';
            return "\"{$this->getName()} {$this->getFrequency()} {$this->getPrice()}$\" owned by \"{$owner}\"";
        }

    }