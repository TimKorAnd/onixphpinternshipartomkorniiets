<?php


    namespace lesson3\app\Models\products;

    use Iterator;
    use lesson3\app\Helpers\Logger;
    use lesson3\app\Helpers\Psr\Log\LoggerAwareInterface;
    use lesson3\app\Helpers\Psr\Log\LoggerInterface;
    use lesson3\app\Models\users\User;
    use Throwable;
    use Traversable;

    abstract class Product implements LoggerAwareInterface
    {
// one static logger for all products is it ok? (was forced to make static method setLogger() in Psr/Log interface)
        protected static LoggerInterface $logger;

        private static array $products; // in domain known as Register

        private string $name;
        private int $price;
        private ?User $owner; // may be without owner? think yea.
        private int $creationTime;
        private int $registrationTime;

        /**
         * Product constructor.
         * @param string $name
         * @param int $price
         * @param User|null $owner
         */

        public function __construct(string $name, int $price, ?User $owner = null)
        {
            $this->name = $name;
            $this->price = $price;
            $this->owner = $owner;
            $this->creationTime = time();
            //self::initializeStaticFields(); now initialization being going in autoloader
        }

        /** Return pseudo random obj
         * @param User $user
         * @param string $classProduct
         * @return Product
         */
        public static function createRandomProduct(User $user, string $classProduct = ''): Product
        {
            $randomIndex = mt_rand(0,1);
            if ($classProduct === ''){
                $classProduct = __NAMESPACE__."\\".['Ram','Processor'][$randomIndex];
            }

            return $classProduct::createProduct([[uniqid('DDR'), 2 ** rand(8, 16), uniqid('memory'), rand(10, 1000), $user],
              [rand(10,1000)*100, uniqid('proc'), 2**rand(8,16),$user]][$randomIndex]);

        }

        /** instantiates accessors classes instances with private class constructor
         * if make not abstract and inherit this method - working fine only with accessor's public constructors
         * @param array $params
         * @return static
         */
        public abstract static function createProduct(Array $params) : ?self;
        /*{
            return new static(...$params);
        }*/

        /** cheet-function for registration via private static registerProduct
         * @param Product $product
         */
        protected function registration(Product $product)
        {
            self::registerProduct($product);
        }

        /**
         * @param Product $product
         * @return bool
         */
        private static function registerProduct(Product $product): bool
        {
            //TODO is it necessary to check $product for NULL if used not nullable type-hinting?
            //self::initializeStaticFields(); now initialization being going in autoloader

            if (!Product::isProductUniqueInRegister($product)) {
                self::$logger->log('notice',
                  '[{time}]: Trying to register {product} already registered early at {registrationTime}!',
                  [
                    'product' => $product->__toString(), //TODO better whole obj?
                    'time' => time(),
                    'registrationTime' => $product->getRegistrationTime()
                  ]);
                return false;
            }

            if (!Product::addProduct($product)) {
                self::$logger->log('error', '[{time}]: {product} has not been registered!',
                  [
                    'product' => $product->__toString(), //TODO better whole obj?
                    'time' => time(),
                  ]);
                return false;
            }
            $product->setRegistrationTime(time());
            self::$logger->log('info', '[{time}]: {product} successful have being added to Register!',
              [
                'product' => $product->__toString(), //TODO better whole obj?
                'time' => time(),
              ]);
            return true;
        }

        /** TODO do we need checks here
         * @param Product $product
         * @return bool If error adding occurred returning false
         */
        private static function addProduct(Product $product): bool
        {
            try {
                self::$products[] = $product;
                return true;
            } catch (Throwable $ex) {
                self::$logger->log('critical', '[{time}]: {exception} happened. {product} has not been added!',
                  [
                    'product' => $product->__toString(), //TODO better whole obj?
                    'time' => time(),
                    'exception' => $ex->getMessage(),
                  ]);
                return false;
            }
        }

        /** Checking product uniqueness
         * @param Product $product
         * @return bool If true - product is unique & vice versa
         */
        private static function isProductUniqueInRegister(Product $product): bool
        {
            return !in_array($product, self::$products, true);
        }

        /**
         * Init static variables : logger & products
         */
        public static function initializeStaticFields(): void
        {
            if (!isset(self::$logger)) {
                self::setLogger(new Logger('defaultProductsLogger', time()));
            }
            if (!isset(self::$products)) {
                self::$products = [];
            }
        }

        /**
         * TODO Is worth magic method to declare as abstract for its mandatory implementation by the heirs?
         * @return string
         */
        public function __toString(): string
        {
            return static::class . " " .
              $this->getName() . " " .
              $this->getPrice() . " " .
              ($this->getOwner())->getUsername();
        }

        /**
         * @return Traversable
         */
        public static function getProductsIterator(): Traversable
        {
            return new class(self::$products) implements Iterator {
                private int $index;
                private array $products;

                /**
                 *  constructor.
                 * @param array $products
                 */
                public function __construct(array $products)
                {
                    $this->index = 0;
                    $this->products = $products;
                }


                public function current(): Product
                {
                    return $this->products[$this->index];
                }

                public function next()
                {
                    ++$this->index;
                }

                public function key()
                {
                    return $this->index;
                }

                public function valid()
                {
                    return isset($this->products[$this->index]);
                }

                public function rewind()
                {
                    $this->index = 0;
                }
            };
        }

//******************** Setters ****************************


        /**
         * @param int $registrationTime
         */
        protected function setRegistrationTime(int $registrationTime): void
        {
            $this->registrationTime = $registrationTime;
        }

        /**
         * @param User $owner
         * @return User
         */
        public function setOwner(User $owner): User
        {
            return $this->owner = $owner;
        }


        public static function setLogger(LoggerInterface $logger): bool
        {
            if (!isset(self::$logger)) {
                self::$logger = $logger;
                self::$logger->log('notice',
                  '[{time}]: Class "{class}" logger {logger} was initialized by default',
                  [
                    'class' => self::class,
                    'time' => time(),
                    'logger' => self::$logger,
                  ]);
                return true;
            }
            self::$logger->log('notice',
              '[{time}]: Class {class} Trying set new logger while default logger {logger} already set',
              [
                'class' => self::class,
                'time' => time(),
                'logger' => self::$logger,
              ]);
            return false;
        }


//************ Getters *******************

        /**
         * //TODO to make public?
         * get time when product have being registered
         * @return int
         */
        private function getRegistrationTime(): int
        {
            return $this->registrationTime;
        }

        /**
         * @return string
         */
        public function getName(): string
        {
            return $this->name;
        }

        /**
         * @return int
         */
        public function getPrice(): int
        {
            return $this->price;
        }

        /**
         * @return User
         */
        public function getOwner(): ?User
        {
            return $this->owner;
        }

    }