<?php


    namespace lesson3\app\Models\products;


    use lesson3\app\Models\users\User;

    class Ram extends Product
    {
        private string $type;
        private int $memory;

        /**
         * Processor constructor.
         * @param string $type
         * @param int $memory
         * @param string $name
         * @param int $price
         * @param User|null $owner
         */
        //TODO does it make sens to apply destructuring for many parameters?
        /**
         * Ram constructor.
         * @param string $type
         * @param int $memory
         * @param string $name
         * @param int $price
         * @param User|null $owner
         */
        private function __construct(string $type, int $memory, string $name, int $price, ?User $owner = null)
        {
            parent::__construct($name, $price, $owner);
            $this->type = $type;
            $this->memory = $memory;

            parent::$logger->log('alert',
              '{name} {type} {memory} created',
              ['name' => $name, 'type' => $type, 'memory' => $memory]);

            $this->registration($this);
        }

        /** instantiates instance with private class constructor))
         * @param array $params
         * @return static
         */
        public static function createProduct(Array $params) : ?self
        {
            try {
                [$type, $memory, $name, $price, $owner] = $params;
                return new static($type, $memory, $name, $price, $owner);
            } catch (\Throwable $ex){
                parent::$logger->log('error',
                  'Params for create Ram obj  {params} is incorrect',
                  ['params' => json_encode($params)]);
                return null;
            }
        }

        /**
         * @return string
         */
        public function getType(): string
        {
            return $this->type;
        }

        /**
         * @return int
         */
        public function getMemory(): int
        {
            return $this->memory;
        }


        public function __toString(): string
        {
            $owner = $this->getOwner() ? $this->getOwner()->getUsername() : 'no owner yet';
            return "\"{$this->getName()} {$this->getType()} {$this->getMemory()} {$this->getPrice()}$\" owned by \"{$owner}\"";
        }


    }