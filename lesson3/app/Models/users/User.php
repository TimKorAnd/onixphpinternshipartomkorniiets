<?php
    namespace lesson3\app\Models\users;

    use Exception;
    use lesson3\app\Controllers\MoneyController;
    use lesson3\app\Controllers\ProductController;
    use lesson3\app\Helpers\Logger;
    use lesson3\app\Helpers\Psr\Log\LoggerAwareInterface;
    use lesson3\app\Helpers\Psr\Log\LoggerInterface;
    use lesson3\app\Models\products\Product;
    use lesson3\app\Models\products\ProductTransaction;

    final class User extends MoneyOperator implements MoneyTransaction, ProductTransaction, LoggerAwareInterface
    {
        private ?MoneyController $moneyController; //TODO move to abstract MoneyOperator
        private ?ProductController $productController; //TODO move to abstract MoneyOperator

        protected static LoggerInterface $logger;

        /**
         * User constructor.
         * @param string $username
         * @param int $balance
         * @param MoneyController|null $moneyController
         * @param ProductController|null $productController
         */
        public function __construct(string $username, int $balance,
                                    ?MoneyController $moneyController = null,
                                    ?ProductController $productController = null)
        {

            //self::initializeStaticFields(); now initialization being going in autoloader
            parent::__construct($username, $balance, false);

            $this->moneyController = $moneyController ?? new MoneyController(self::$logger);
            $this->productController = $productController ?? new ProductController(self::$logger);

            self::$logger->log('info','[{time}]: {user} successful have being created!',
              [
                'user' => $this,//$username->__toString(), //TODO better whole obj?
                'time' => time(),
              ]);
        }

        /**
         * Init static variables : logger & products
         */
        public static function initializeStaticFields(): void
        {
            if (!isset(self::$logger)) {
                self::setLogger(new Logger('defaultUsersLogger', time()));
            }
        }

        /** Return string-view of user obj
         * @return string
         */
        public function __toString(): string
        {
            return "\"{$this->getUsername()}\" with balance \"{$this->getBalance()}$\"";
        }

        /** get username (TODO redundant, remove)
         * @return string
         */
        public function getUsername(): string
        {
            return $this->getName();
        }

        /**
         * @return \app\Controllers\MoneyController|null
         */
        public function getMoneyController(): ?MoneyController
        {
            return $this->moneyController;
        }

        /**  TODO return array or iterator being a best practice?
         * Return array of all this user's products
         * @return iterable ?? array
         */
        public function listProducts(): iterable
        {
            return array_reduce(iterator_to_array(Product::getProductsIterator(), FALSE),
              function (array $carry, Product $product) {
                  return $product->getOwner() === $this ?
                    [...$carry, $product] : [...$carry];
              },
              []);
        }



        /** get balance of user
         * @return int
         */
        public function getBalance(): int
        {
            return parent::getBalance();
        }

        public static function setLogger(LoggerInterface $logger): bool
        {
            if (!isset(self::$logger)) {
                self::$logger = $logger;
                self::$logger->log('notice',
                  '[{time}]: Class "{class}" logger {logger} was initialized by default',
                  [
                    'class' => self::class,
                    'time' => time(),
                    'logger' => self::$logger,
                  ]);
                return true;
            }
            self::$logger->log('notice',
              '[{time}]: Class {class} Trying set new logger while default logger {logger} already set',
              [
                'class' => self::class,
                'time' => time(),
                'logger' => self::$logger,
              ]);
            return false;
        }

        function giveMoney(MoneyOperator $userRecipient, int $paymentAmount): bool
        {
            if (!$this->moneyController->giveMoneyTransactionFullCheck($this, $userRecipient, $paymentAmount)) {
                self::$logger->log('error','[{time}]: {user} giveMoney transaction failed',
                  [
                    'user' => $userRecipient,
                    'time' => time(),
                  ]);
                return false;
            }
            $reservedUserBalance = $this->getBalance();
            $reservedUserRecipientBalance = $userRecipient->getBalance();
            try {
                $this->setBalance($this->getBalance() - $paymentAmount);
                $userRecipient->setBalance($userRecipient->getBalance() + $paymentAmount);
                self::$logger->log('error','[{time}]: User <{userPayer}> transferred money: <{paymentAmount}>$ to user <{userRecipient}>',
                  [
                    'userPayer' => $this->getName(),
                    'userRecipient' => $userRecipient->getName(),
                    'paymentAmount' => $paymentAmount,
                    'time' => time(),
                  ]);
                return true;
            } catch (Exception $ex) {
                $this->setBalance($reservedUserBalance);
                $userRecipient->setBalance($reservedUserRecipientBalance);
                self::$logger->log('critical','[{time}]: transaction error occurred.',
                  [
                    'time' => time(),
                  ]);
                throw $ex;
            }
        }

        /** hardcode(( need refactoring for this method
         * @param User $buyer
         * @param Product $product
         * @return bool
         * @throws Exception
         */
        function sellProduct(User $buyer, Product $product): bool
        {
            if ($product->getOwner() !== $this) {
                self::$logger->log('notice','[{time}]: {user} hav\'t product {product}].',
                  [
                    'time' => time(),
                    'user' => $this,
                    'product' => $product,
                  ]);
                return false;
            }
            try {
                if ($buyer->giveMoney($this, $product->getPrice()) && $product->setOwner($buyer)) {
                    self::$logger->log('info','[{time}]: User <{buyer}> buy <{product}> from user <{userSeller}> for {price}$>',
                      [
                        'buyer' => $buyer,
                        'userSeller' => $this,
                        'product' => $product,
                        'price' => $product->getPrice(),
                        'time' => time(),
                      ]);
                    return true;
                } else {
                    self::$logger->log('info','[{time}]: User <{buyer}> can\'t buy <{product}> from user <{userSeller}> for {price}$>',
                      [
                        'buyer' => $buyer,
                        'userSeller' => $this,
                        'product' => $product,
                        'price' => $product->getPrice(),
                        'time' => time(),
                      ]);
                    return false;
                }
            } catch (\Exception $ex) {
                //TODO log error
                Throw($ex);
            }

        }
    }