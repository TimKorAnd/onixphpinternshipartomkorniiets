<?php

    namespace lesson3\app\Models\users;


    /**
     * Interface MoneyTransaction
     * @package app\Models
     */
    interface MoneyTransaction
    {
        function giveMoney(MoneyOperator $userRecipient, int $paymentAmount): bool;

        //function takeMoney(MoneyOperator $fromUser, int $amountOfMoney): bool;

        //function __toString(): string;
    }