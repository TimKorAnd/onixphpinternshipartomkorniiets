<?php

    namespace lesson3\app\Models\users;


    abstract class MoneyOperator
    {
        private string $name;
        private int $balance;
        private bool $isBank;

        /**
         * MoneyOperator constructor.
         * @param string $name
         * @param int $balance
         * @param bool $isBank
         */
        public function __construct(string $name, int $balance, bool $isBank = false)
        {
            $this->name = $name;
            $this->balance = $balance;
            $this->isBank = $isBank;
        }


        /**
         * @return string
         */
        protected function getName(): string
        {
            return $this->name;
        }

        /**
         * @return int
         */
        protected function getBalance(): int
        {
            return $this->balance;
        }

        /**
         * @param int $balance
         * @return bool
         */
        protected function setBalance(int $balance): bool
        {
            // TODO check & control needed here or in heirs!?
            $this->balance = $balance;
            return true;
        }

        /**
         * @return bool
         */
        public function isBank(): bool
        {
            return $this->isBank;
        }
    }