<?php

    namespace lesson3\app\Models\users;


    /**
     * singleton src = https://designpatternsphp.readthedocs.io/en/latest/Creational/Singleton/README.html
     * Class Banker singleton for money start transaction to users;
     */
    final class Bank extends MoneyOperator implements MoneyTransaction
    {
        //private string $name;
        private int $creditAmount;
        private int $debitAmount;
        private int $balance;
        private array $creditors; //TODO how arrays elements type hinting?
        private static ?Bank $instance = null;

        private function __construct(string $name, int $balance)
        {
            parent::__construct($name, $balance, true);
            /*$this->name = $name;
            $this->balance = $balance;*/
            $this->printStatus();
        }

        public static function getBank(string $name, int $balance): Bank
        {
            if (static::$instance === null) {
                static::$instance = new static($name, $balance);
                echo 'bank is created' . "\n";
            } else {
                $name = static::getBankName();
                echo "\n Bank monopolist {$name} was created early \n";
            }

            return static::$instance;
        }

        public static function isBankExist(): bool
        {
            return !(static::$instance === null);
        }

        /**
         * prevent the instance from being cloned (which would create a second instance of it)
         */
        private function __clone()
        {
        }

        /**
         * prevent from being unserialized (which would create a second instance of it)
         */
        private function __wakeup()
        {
        }

        public function giveMoney(MoneyOperator $userRecipient, int $paymentAmount): bool
        {
            // TODO: Implement giveMoney() method.
            echo 'bank giveMoney' . "\n";
        }

        public function printStatus(): bool
        {
            // TODO: Implement printStatus() method.
            echo "\n" . get_class() . " {$this->getName()} has {$this->getBalance()} money on balance.\n";
            return true;
        }

        public static function getBankName(): string
        {
            return static::$instance->getName();
        }

        function takeMoney(MoneyOperator $fromUser, int $amountOfMoney): bool
        {
            //if($this->balanceAdequacyCheckingForPayment())
            // TODO: Implement takeMoney() method.
        }

        protected function getBalance(): int
        {
            // TODO: Implement getBalance() method.
            return $this->balance;
        }
    }