<?php

    namespace lesson3\app\Controllers;

    use lesson3\app\Helpers\Psr\Log\LoggerInterface;
    use lesson3\app\Models\users\Bank;
    use lesson3\app\Models\users\MoneyOperator;
    use lesson3\app\MoneyExceptions\MoneyException;
    use lesson3\app\Models\users\User;


    /**
     * Class MoneyController
     * Logic for money & transaction control.
     * @package app\Controllers
     */
    class MoneyController
    {
        private int $initBalanceMin;
        private int $initBalanceMax;
        private int $balanceMin;
        private int $balanceMax;
        private int $paymentMin;
        private int $paymentMax;
        private bool $bankMonopoly; //if true then positive balance can't be created, monet may take from bank only
        private int $initBalanceBankAllowed;
        //TODO transaction limits ? int math round($f, 2) s
        /**
         * @var LoggerInterface
         */
        private LoggerInterface $logger;

        /**
         * MoneyController constructor.
         * @param LoggerInterface $logger
         * @param bool $bankMonopoly - true prohibits creating users with a balance bigger than banks allowed if a bank exists
         * perhaps this parameter should be stored in the bank
         */
        public function __construct(LoggerInterface $logger,$bankMonopoly = false)
        {
            $this->logger = $logger;
            $this->bankMonopoly = $bankMonopoly;
            $this->initBalanceMin = 0;
            $this->initBalanceMax = PHP_INT_MAX;
            //$this->balanceMin = -PHP_INT_MAX; // negative balance is allowed
            $this->balanceMin = 0; // negative balance is prohibited
            $this->balanceMax = PHP_INT_MAX;
            $this->paymentMin = 0;
            $this->paymentMax = PHP_INT_MAX;
            $this->initBalanceBankAllowed = 0; //setters for bellow expected

        }


        public function giveMoneyTransactionFullCheck(MoneyOperator $userPayer, MoneyOperator $userRecipient, int $paymentAmount): bool
        {
            if (!($userPayer instanceof User && $userRecipient instanceof User )){
                $this->logger->log('error','[{time}]: {users} one or both are not valid. Only for user participation',
                  [
                    'users' => $userPayer.", ".$userRecipient,
                    'time' => time(),
                  ]);
                return false;
            }
            if ($userPayer === $userRecipient){
                $this->logger->log('error','[{time}]: {user} it is impossible to perform an operation with the same user',
                  [
                    'user' => $userPayer,
                    'time' => time(),
                  ]);
                return false;
            }
            //TODO changed type in params to User from MoneyOperator, else called protected getBallance from there
            if (!$this->checkPayment($paymentAmount)) {
                $this->logger->log('error','[{time}]: {users} bad payment amount',
                  [
                    'users' => $userPayer." to ".$userRecipient,
                    'time' => time(),
                  ]);
                return false;
            }
            if (!$this->checkBalance($userPayer->getBalance() - $paymentAmount)) {
                $this->logger->log('error','[{time}]: {user} bad balance after payment',
                  [
                    'user' => $userPayer,
                    'time' => time(),
                  ]);
                return false;
            };
            if (!$userRecipient->getMoneyController()->checkBalance($userRecipient->getBalance() + $paymentAmount)) {
                $this->logger->log('error','[{time}]: {user} bad balance after payment',
                  [
                    'user' => $userRecipient,
                    'time' => time(),
                  ]);
                return false;
            };
            return true;
        }

        public function checkBalance(int $balance): bool
        {
            return ($balance >= $this->balanceMin && $balance <= $this->balanceMax);
        }

        public function checkPayment(int $payment): bool
        {
            return ($payment >= $this->paymentMin && $payment <= $this->paymentMax);
        }

        public function checkInitBalance(int $initBalance): bool
        {
            if (!($initBalance >= $this->initBalanceMin && $initBalance <= $this->initBalanceMax)) {
                throw new MoneyException("Initial balance {$initBalance} is out of limits:\nfrom min = {$this->initBalanceMin} to max = {$this->initBalanceMax}");
            }
            return true;
        }

        public function getInitBalanceBankAllowed(int $initBalance, string $name): int
        {
            if ($this->bankMonopoly && Bank::isBankExist()) {
                if (round($initBalance, 2) > round($this->initBalanceBankAllowed, 2)) {
                    $bankName = Bank::getBankName();
                    //TODO all echo msg in constants.
                    $this->logger->log('error','[{time}]:!!! New created user "{userName}" can\'t take balance "{initBalance}" now,
                    because the "{bankName}" bank already exist. Contact the {bankName} to receive money.',
                      [
                        'userName' => $name,
                        'time' => time(),
                        'initBalance' => $initBalance,
                        'bankName' => $bankName,

                      ]);

                    $initBalance = $this->initBalanceBankAllowed;
                }
            }
            return $initBalance;
        }

        /**
         * @param int|int $initBalanceMin
         */
        public function setInitBalanceMin($initBalanceMin): MoneyController
        {
            $this->initBalanceMin = $initBalanceMin;
            return $this;
        }

        /**
         * @param int $initBalanceMax
         */
        public function setInitBalanceMax(int $initBalanceMax): MoneyController
        {
            $this->initBalanceMax = $initBalanceMax;
            return $this;
        }

        /**
         * @param int|int $balanceMin
         */
        public function setBalanceMin($balanceMin): MoneyController
        {
            $this->balanceMin = $balanceMin;
            return $this;
        }

        /**
         * @param int $balanceMax
         */
        public function setBalanceMax(int $balanceMax): MoneyController
        {
            $this->balanceMax = $balanceMax;
            return $this;
        }

        /**
         * @param int|int $paymentMin
         */
        public function setPaymentMin($paymentMin): MoneyController
        {
            $this->paymentMin = $paymentMin;
            return $this;
        }

        /**
         * @param int|int $initBalanceBankAllowed
         */
        public function setInitBalanceBankAllowed($initBalanceBankAllowed): MoneyController
        {
            $this->initBalanceBankAllowed = $initBalanceBankAllowed;
            return $this;
        }

        /**
         * @param int $paymentMax
         */
        public function setPaymentMax(int $paymentMax): MoneyController
        {
            $this->paymentMax = $paymentMax;
            return $this;
        }

    }