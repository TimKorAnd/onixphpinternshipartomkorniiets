<?php


    namespace lesson3\app\Helpers;


    class TimeHelper
    {
        private static int $maxDelay = 0;
        /**
         * imitate pause for logging at different times
         */
        public static function pseudoPause(): void
        {
            $temp = rand(0, self::$maxDelay);
            echo "\n".'(ponaroshku:) please wait for connection:';
            while($temp-- > 0){
                sleep(1);
                echo '.';
                sleep(1);
            }
            echo "done!";

        }

    }