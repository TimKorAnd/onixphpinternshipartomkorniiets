<?php
    namespace lesson3\app\MoneyExceptions;

    class MoneyException extends \Exception
    {
        /**
         * MoneyException constructor.
         * @param String $exceptionMsg
         */
        public function __construct(string $exceptionMsg)
        {
            parent::__construct($exceptionMsg);
        }
    }