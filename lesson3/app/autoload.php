<?php
    namespace lesson3\app;

spl_autoload_register(function($class) {
    require dirname(__DIR__,2) . DIRECTORY_SEPARATOR . str_replace('\\',
        DIRECTORY_SEPARATOR, $class) . '.php';

    //static fields common initializer method checked and run
    if (method_exists($class, 'initializeStaticFields')){
        $class::initializeStaticFields();
    }
});
