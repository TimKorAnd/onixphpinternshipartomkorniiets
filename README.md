changelog:
- initializeStaticFields() - now initialization being going in autoloader.  I had to declare this as public.
- in Ram & Processor classes constructors declared as private. instantiation occurs through the overridden static method 
from the parent class Products: 
    public abstract static function createProduct(Array $params) : self; //**

Interestingly, constructor of parent class Product is public, successors classes - private.

//** 
in first variant this method was not abstract in parent class Products,  and all working fine , but with public Ram & Proc 
constructors only:
    public static function createProduct(Array $params) : self;
    {
        return new static(...$params);
    }
 

- pseudo random return one object;
- in_array searching exchanged by field compare in User class:
    function sellProduct(User $buyer, Product $product): bool
    {
        if ($product->getOwner() !== $this) {...} 
        ...
    }    

+Скопируйте код классов из предыдущего задания в файл ‘lesson3.php’, добавьте для него неймспейс
+Реализуйте в классе User публичный метод listProducts который возвращает все продукты данного пользователя из регистра
Реализуйте в классе User публичный метод который должен менять владельца продукта и передавать сумму в 
размере цены продукта от того пользователя который покупает тому пользователю который продает.
Если у пользователя недостаточно денег на балансе чтобы купить продукт, то выводим сообщение “Пользователь <name> не 
может перечислить $<amount> пользователю <name> так как имеет только $<balance>”. 
Продавать можно только свои продукты, если пользователь пытается продать не свой продукт то выводим сообщение 
“Пользователь <name> не может продать продукт <name> ($<price>)  так как он принадлежит <name>”
При успешной продаже вывести сообщение “Пользователь <name> продал продукт <name> ($<price>) пользователю <name>”
Сделать функцию registerProduct приватной и сделать создание продукта невозможным без регистрации
Реализовать в классе Product публичную статическую функцию createRandomProduct которая принимает пользователя и работает
 как фабрика которая создает для него случайный продукт (для генерации случайных строковых параметров можно использовать
  функцию uniqid)
Уведомить руководителя интернатуры о выполненном задании сразу после выполнения, скорость в нашей работе важна

========================================================================================================================

lesson 2:
(новый создал) Скопируйте код класса User из предыдущего задания в файл ‘lesson2.php’.
Добавьте в файл ‘lesson1.php’ и ‘lesson2.php’ неймспейсы, чтобы убрать уведомление о дублирующей декларации классов
Заменить функцию printStatus на магический метод  __toString.
Огласите абстрактный класс Product с приватными полями: name, price и owner (ссылка на объект User)
Огласите клас Processor который наследуется от класса Product с приватным полем frequency
Огласите клас Ram который наследуется от класса Product с приватными полями type и memory
Добавьте в класс Product статичное приватное поле products в котором должны храниться все созданные продукты 
(как Processors так и Ram). Это будет регистр продуктов
В классе Product реализуйте публичный статический метод registerProduct который будет добавлять продукт в регистр 
продуктов
Нельзя добавить один и тот же продукт два раза (проверка только по ссылке класса)
Реализуйте публичный статический метод который возвращает итератор по регистру продуктов. Итератор реализуйте анонимным
 классом. Зарегистрируйте несколько продуктов c их владельцами а потом выведите их имена и цены и имена владельцев через
проход по итератору. Вывод делать через функцию __toString. 
Уведомить руководителя интернатуры о выполненном задании сразу после выполнения, скорость в нашей работе важна

lesson2/app/Helpers/Psr/Log - Psr-3 logger used there (Test not used)


========================================================================================================================


lesson 1:
Реализовать класс User у которого есть приватные свойства name и balance (сумма денег на счету)
Реализовать публичный метод printStatus который выводит сообщение следующего формата “У пользователя <name> сейчас на
счету $<balance>” Реализовать публичный метод giveMoney который передает деньги от одного пользователя к другому (нельзя
передать больше денег чем есть на балансе). 
Во время передачи денег выводится сообщение “Пользователь <name> перечислил $<amount> пользователю <name>”
Пользователю нельзя изменить баланс любым другим способом кроме giveMoney
Создать 2 экземпляра класса User с разными балансами, вывести состояние счета каждого из них, совершить передачу денег
от одного к другому, снова вывести состояние счета каждого из них.
Все обращения к полям должны быть через геттеры
Запушить созданный файл ‘lesson1.php’ в свой репозиторий

файл lesson1.php находится в папке public. Пользователи создаются через фабрику (не абстрактную, не статическую), 
для создания пользователя нужно указать его имя, стартовый баланс, а также опционально объект-контроллер денег. Также 
можно создавать банк , но только один. Если банк создан, то стартовый баланс пользователя зависит от параметра его
контроллера денег, возможно правильно этот параметр перенести в банк. 

 - Не решил как запретить создавать пользователя не через фабрику.
 - Не вынес все сообщения в отдельный класс.
 +(lesson1/app/Controllers/MoneyController.php:58) 
 решил полиморфическую проблему с вызовом переопределенного метода передаваемого параметром объекта, если тип 
параметра родительский абстрактный класс. - проверка типа объекта исправляет ситуацию.

