<?php // TODO code style reformat

    declare(strict_types=1);

    namespace lesson2\app;

    use lesson2\app\Helpers\TimeHelper;
    use lesson2\app\Models\Processor;
    use lesson2\app\Models\Product;
    use lesson2\app\Models\Ram;
    use lesson2\app\Models\User;

    require '../app/autoload.php';

    date_default_timezone_set('Europe/Uzhgorod');



    echo "\nScript start at: ".date('Y-m-d H:i:s',time());

    $userVasiliy = new User('Vasiliy');
    $userBill = new User('Bill Gates');

    $p = new Processor( 2500, 'AMD Ryzer 3', 150, $userVasiliy);

    $p1 = new Processor( 3000, 'AMD Ryzer 5', 250);
    //$p1->setOwner($userVasiliy);

    $p2 = new Processor( 4000, 'Intel i9', 2500);
    $p2->setOwner($userBill);

    $r = new Ram('DDR4',16, 'WD',85);
    $r1 = new Ram('DDR3',4, 'Sclerozz',12, new User('Old School'));
    $r2 = new Ram('DDR5',32, 'Samsung',260);
    $r2->setOwner(new User('Tim Kor'));

    Product::registerProduct($p);
    $duplicate = $p;
    TimeHelper::pseudoPause();
    Product::registerProduct($duplicate);
    TimeHelper::pseudoPause();
    Product::registerProduct($p1);
    TimeHelper::pseudoPause();
    Product::registerProduct($p2);
    TimeHelper::pseudoPause();
    Product::registerProduct($p1);
    TimeHelper::pseudoPause();
    Product::registerProduct($r);
    TimeHelper::pseudoPause();
    Product::registerProduct($r1);
    TimeHelper::pseudoPause();
    Product::registerProduct($r);
    TimeHelper::pseudoPause();
    Product::registerProduct($r2);
    TimeHelper::pseudoPause();
    Product::registerProduct($r);
    TimeHelper::pseudoPause();

    echo "\nIteration over all registered products:";
    $iter = Product::getProductsIterator();
    foreach($iter as $key => $val){
        echo "\n".$key.": ".$val;
    }

    echo "\n\nScript completed at: ".date('Y-m-d H:i:s',time());