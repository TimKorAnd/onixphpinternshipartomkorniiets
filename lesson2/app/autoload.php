<?php
    namespace lesson2\app;

spl_autoload_register(function($class) {
    // levels 2 - is used to explicit namespace specify in the classes being required
    /*echo dirname(__DIR__,2) . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php'.PHP_EOL;
    echo DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php'.PHP_EOL;*/
    require dirname(__DIR__,2) . DIRECTORY_SEPARATOR . str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
});
