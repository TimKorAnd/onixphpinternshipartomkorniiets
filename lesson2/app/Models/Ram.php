<?php


    namespace lesson2\app\Models;


    class Ram extends Product
    {
        private string $type;
        private int $memory;

        /**
         * Processor constructor.
         * @param string $type
         * @param int $memory
         * @param string $name
         * @param int $price
         * @param User|null $owner
         */
        //TODO does it make sens to apply destructuring for many parameters?
        public function __construct(string $type, int $memory, string $name, int $price, ?User $owner = null)
        {
            parent::__construct($name, $price, $owner);
            $this->type = $type;
            $this->memory = $memory;

            parent::$logger->log('alert',
              '{name} {type} {memory} created',
              ['name' => $name, 'type' => $type, 'memory' => $memory]);
        }

        /**
         * @return string
         */
        public function getType(): string
        {
            return $this->type;
        }

        /**
         * @return int
         */
        public function getMemory(): int
        {
            return $this->memory;
        }


        public function __toString(): string
        {
            $owner = $this->getOwner() ? $this->getOwner()->getUsername() : 'no owner yet';
            return "\"{$this->getName()} {$this->getType()} {$this->getMemory()} {$this->getPrice()}$\" owned by \"{$owner}\"";
        }
    }