<?php


    namespace lesson2\app\Models;


    class Processor extends Product
    {
        private int $frequency;

        /**
         * Processor constructor.
         * @param int $frequency
         * @param string $name
         * @param int $price
         * @param User|null $owner
         */
        public function __construct(int $frequency, string $name, int $price, ?User $owner = null)
        {
            parent::__construct($name, $price, $owner);
            //$this->setCreationTime(time());
            $this->frequency = $frequency;

            parent::$logger->log('alert',
              '{name} {frequency} created',
              ['name' => $name, 'frequency' => $frequency]);
        }

        /**
         * @return int
         */
        public function getFrequency(): int
        {
            return $this->frequency;
        }



        public function __toString(): string
        {
            $owner = $this->getOwner() ? $this->getOwner()->getUsername() : 'no owner yet';
            return "\"{$this->getName()} {$this->getFrequency()} {$this->getPrice()}$\" owned by \"{$owner}\"";
        }

    }