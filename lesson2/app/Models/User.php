<?php
    namespace lesson2\app\Models;

    final class User
    {
        private string $username;

        /**
         * User constructor.
         * @param string $username
         */
        public function __construct(string $username)
        {
            $this->username = $username;
        }


        public function __toString(): string
        {
            return "\"{$this->getUsername()}\"";
        }

        /** get username
         * @return string
         */
        public function getUsername(): string
        {
            return $this->username;
        }


    }