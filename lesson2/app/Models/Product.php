<?php


    namespace lesson2\app\Models;

    use lesson2\app\Helpers\Logger;
    use lesson2\app\Helpers\Psr\Log\LoggerAwareInterface;
    use lesson2\app\Helpers\Psr\Log\LoggerInterface;
    use Throwable;
    use Traversable;

    abstract class Product implements LoggerAwareInterface
    {
// one static logger for all products is it ok? (was forced to make static method setLogger() in Psr/Log interface)
        protected static LoggerInterface $logger;

        private static array $products; // in domain known as Register

        private string $name;
        private int $price;
        private ?User $owner; // may be without owner? think yea.
        private int $creationTime;
        private int $registrationTime;

        /**
         * Product constructor.
         * @param string $name
         * @param int $price
         * @param User|null $owner
         */
        public function __construct(string $name, int $price, ?User $owner = null)
        {
            $this->name = $name;
            $this->price = $price;
            $this->owner = $owner;
            $this->creationTime = time();
            self::initialiseStaticVariables();
        }

        /**
         * @param Product $product
         * @return bool
         */
        public static function registerProduct(Product $product): bool
        {
            //TODO is it necessary to check $product for NULL if used not nullable type-hinting?
            self::initialiseStaticVariables();

            if (!Product::isProductUniqueInRegister($product)) {
                self::$logger->log('notice',
                  '[{time}]: Trying to register {product} already registered early at {registrationTime}!',
                  [
                    'product' => $product->__toString(), //TODO better whole obj?
                    'time' => time(),
                    'registrationTime' => $product->getRegistrationTime()
                  ]);
                return false;
            }

            if (!Product::addProduct($product)) {
                self::$logger->log('error', '[{time}]: {product} has not been registered!',
                  [
                    'product' => $product->__toString(), //TODO better whole obj?
                    'time' => time(),
                  ]);
                return false;
            }
            $product->setRegistrationTime(time());
            self::$logger->log('info', '[{time}]: {product} successful have being added to Register!',
              [
                'product' => $product->__toString(), //TODO better whole obj?
                'time' => time(),
              ]);
            return true;
        }

        /** TODO do we need checks here
         * @param Product $product
         * @return bool If false returned than
         */
        private static function addProduct(Product $product): bool
        {
            try {
                self::$products[] = $product;
                return true;
            } catch(Throwable $ex){
                self::$logger->log('critical', '[{time}]: {exception} happened. {product} has not been added!',
                  [
                    'product' => $product->__toString(), //TODO better whole obj?
                    'time' => time(),
                    'exception' => $ex->getMessage(),
                  ]);
                return false;
            }
        }

        /** Checking product uniqueness
         * @param Product $product
         * @return bool If true - product is unique & vice versa
         */
        private static function isProductUniqueInRegister(Product $product): bool
        {
            return !in_array($product, self::$products, true);
        }

        /**
         * Init static variables : logger & products
         */
        private static function initialiseStaticVariables(): void
        {
            if (!isset(self::$logger)) {
                self::setLogger(new Logger('defaultLogger', time()));
            }
            if (!isset(self::$products)) {
                self::$products = [];
            }
        }

        /**
         * TODO Is worth magic method to declare as abstract for its mandatory implementation by the heirs?
         * @return string
         */
        public function __toString(): string
        {
            return static::class." ".
              $this->getName()." ".
              $this->getPrice()." ".
              ($this->getOwner())->getUsername();
        }

        /**
         * @return Traversable
         */
        public static function getProductsIterator(): Traversable {
            return new class(self::$products) implements \Iterator {
                private int $index;
                private array $products;

                /**
                 *  constructor.
                 * @param array $products
                 */
                public function __construct(array $products)
                {
                    $this->index = 0;
                    $this->products = $products;
                }


                public function current(): Product
                {
                    return $this->products[$this->index];
                }

                public function next()
                {
                    ++$this->index;
                }

                public function key()
                {
                    return $this->index;
                }

                public function valid()
                {
                    return isset($this->products[$this->index]);
                }

                public function rewind()
                {
                    $this->index = 0;
                }
            };
        }

//******************** Setters ****************************
        /**
         * @param int $registrationTime
         */
        protected function setRegistrationTime(int $registrationTime): void
        {
            $this->registrationTime = $registrationTime;
        }

        /**
         * @param User $owner
         * @return User
         */
        public function setOwner(User $owner): User
        {
            return $this->owner = $owner;
        }


        public static function setLogger(LoggerInterface $logger): bool
        {
            if (!isset(self::$logger)) {
                self::$logger = $logger;
                self::$logger->log('notice',
                  '[{time}]: Class "{class}" logger {logger} was initialized by default',
                  [
                    'class' => self::class,
                    'time' => time(),
                    'logger' => self::$logger,
                  ]);
                return true;
            }
            self::$logger->log('notice',
              '[{time}]: Class {class} Trying set new logger while default logger {logger} already set',
              [
                'class' => self::class,
                'time' => time(),
                'logger' => self::$logger,
              ]);
            return false;
        }


//************ Getters *******************

        /**
         * //TODO to make public?
         * get time when product have being registered
         * @return int
         */
        private function getRegistrationTime(): int
        {
            return $this->registrationTime;
        }

        /**
         * @return string
         */
        protected  function getName(): string
        {
            return $this->name;
        }

        /**
         * @return int
         */
        protected function getPrice(): int
        {
            return $this->price;
        }

        /**
         * @return User
         */
        protected function getOwner(): ?User
        {
            return $this->owner;
        }

    }