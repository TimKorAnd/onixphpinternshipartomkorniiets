<?php


    namespace lesson2\app\Helpers;
    use lesson2\app\Helpers\Psr\Log\AbstractLogger;

    /**
     * Class Logger TODO does it make sense to do a singleton? Think yes.
     * @package lesson2\app\Helpers
     */
    class Logger extends AbstractLogger
    {
        private int $timeOfCreation;
        private string $loggerName; // why not?))

        /**
         * Logger constructor.
         * @param int $timeOfCreation
         * @param string $loggerName
         */
        public function __construct(string $loggerName, int $timeOfCreation)
        {
            $this->timeOfCreation = $timeOfCreation;
            $this->loggerName = $loggerName;
        }


        public function log($level, $message, array $context = array())
        {
            // TODO: Implement log() method.
            echo "\n".'__________________________________________'."\n";
            echo $this->interpolate($message, $context);
        }

        /** Code of this method sourced https://www.php-fig.org/psr/psr-3/
         * Interpolates context values into the message placeholders.
         * @param string $message
         * @param array $context
         * @return string
         */
        function interpolate(string $message, array $context = array())
        {
            // build a replacement array with braces around the context keys
            $replace = array();
            foreach ($context as $key => $val) {
                // check that the value can be cast to string
                if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                    if ($key === 'time' || $key === 'registrationTime'){
                        $val = date('Y-m-d H:i:s',$val);
                    }

                    $replace['{' . $key . '}'] = $val;
                }
            }

            // interpolate replacement values into the message and return
            return strtr($message, $replace);
        }

        public function __toString(): string
        {
            // TODO: Implement __toString() method.
            return "\"".$this->loggerName."\"";
        }
    }