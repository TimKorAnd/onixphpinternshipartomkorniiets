<?php
    declare(strict_types=1);// TODO code style reformat
    //namespace app;
    //TODO figure out namespacees

    use app\Models\Bank;
    use app\Controllers\MoneyController;
    use app\Models\User;
    use app\Models\UserFactory;


    require '../app/autoload.php';
    /*require_once('../Controllers/MoneyController.php');
    require_once('../Models/Bank.php');
    require_once('../Models/User.php');
    require_once('../Models/UserFactory.php');*/

//TODO money need especial class for work with it?
//TODO or using round($m, 2) function is enough? What precision will be sufficient?

    $userMoneyController = (new MoneyController(true))->
    setInitBalanceMin(10.0)->
    setBalanceMin(0.0);

    $userFactory = new UserFactory();
    $u0 = $userFactory->createUser('name_U0', 10.000, $userMoneyController);
    $u0->printStatus();

    $u1 = $userFactory->createUser('name+U1', 15);
    $u1->printStatus();

    $u0->giveMoney($u1, 9.42);
    /*$b0 = Bank::getBank('tkb',1000.00);
    $b0->printStatus();
    $b1 = Bank::getBank('tkb',1000.00);
    echo "is banks equals? ".(($b0 === $b1)?"true":"false");
    $b3 = Bank::getBank('anotherBank',100000.00);
    echo "is banks equals? ".(($b0 === $b3)?"true":"false");*/

    $u0->printStatus();
    $u1->printStatus();
    $u2 = $userFactory->createUser('name_U2', 55.77, (new MoneyController(true))->setInitBalanceBankAllowed(55.76));
