<?php


    namespace app\Models;


    use app\Controllers\MoneyController;
    use app\MoneyExceptions\MoneyException;

    class UserFactory
    {
        public function createUser(string $name, float $balance, ?MoneyController $moneyController = null): ?User
        {
            $moneyController = $moneyController ?? new MoneyController();
            //TODO name check && log
            try {
                $moneyController->initBalanceCheck($balance);
                $balance = $moneyController->getInitBalanceBankAllowed($balance, $name);
                //echo "\n".get_class() . " User \"{$this->getName()}\" created successful!\nHas \"{$this->getBalance()}\" money on balance.\n";
                return new User($name, $balance, $moneyController);
            } catch (MoneyException $me) {
                $this->moneyController = null;
                unset($this->moneyController);
                echo "\n!!!User {$name} not create!\n" . $me->getMessage() . "\n";
                //die;
                throw $me;
            }
            return null;
        }
    }