<?php

    namespace app\Models;
    require_once 'MoneyTransaction.php';
    require_once 'MoneyOperator.php';

    use app\Models\MoneyOperator;
    use app\Models\MoneyTransaction;

    /**
     * singleton src = https://designpatternsphp.readthedocs.io/en/latest/Creational/Singleton/README.html
     * Class Banker singleton for money start transaction to users;
     */
    final class Bank extends MoneyOperator implements MoneyTransaction
    {
        //private string $name;
        private float $creditAmount;
        private float $debitAmount;
        //private float $balance;
        private array $creditors; //TODO how arrays elements type hinting?
        private static ?Bank $instance = null;

        private function __construct(string $name, float $balance)
        {
            parent::__construct($name, $balance, true);
            /*$this->name = $name;
            $this->balance = $balance;*/
            $this->printStatus();
        }

        public static function getBank(string $name, float $balance): Bank
        {
            if (static::$instance === null) {
                static::$instance = new static($name, $balance);
                echo 'bank is created' . "\n";
            } else {
                $name = static::getBankName();
                echo "\n Bank monopolist {$name} was created early \n";
            }

            return static::$instance;
        }

        public static function isBankExist(): bool
        {
            return !(static::$instance === null);
        }

        /**
         * prevent the instance from being cloned (which would create a second instance of it)
         */
        private function __clone()
        {
        }

        /**
         * prevent from being unserialized (which would create a second instance of it)
         */
        private function __wakeup()
        {
        }

        public function giveMoney(MoneyOperator $userRecipient, float $amountOfPaymentAmount): bool
        {
            // TODO: Implement giveMoney() method.
            echo 'bank giveMoney' . "\n";
        }

        public function printStatus(): bool
        {
            // TODO: Implement printStatus() method.
            echo "\n" . get_class() . " {$this->getName()} has {$this->getBalance()} money on balance.\n";
            return true;
        }

        public static function getBankName(): string
        {
            return static::$instance->getName();
        }

        function takeMoney(MoneyOperator $fromUser, float $amountOfMoney): bool
        {
            //if($this->balanceAdequacyCheckingForPayment())
            // TODO: Implement takeMoney() method.
        }

        protected function getBalance(): float
        {
            // TODO: Implement getBalance() method.
        }
    }