<?php

    namespace app\Models;

    use app\Controllers\MoneyController;
    use app\Models;
    use app\MoneyExceptions\MoneyException;

    /**
     * Class User
     * Optionally may contains a money-controller object in charge of constraint logic
     * in theory can only be created by a factory (not realized yet)
     * @package app\Models
     */
    final class User extends MoneyOperator implements MoneyTransaction
    {
        private ?MoneyController $moneyController; //TODO move to abstract MoneyOperator

        //TODO is it real reject direct creation? Enable creation from factory ONLY
        public function __construct(string $name, float $balance, ?MoneyController $moneyController = null)
        {
            $this->moneyController = $moneyController ?? new MoneyController();
            //TODO name check && log there or in factory
            parent::__construct($name, $balance, false);
            $this->notifyCreation();
        }

        /**
         * @param MoneyOperator $userRecipient
         * @param float $paymentAmount
         * @return bool
         * @throws \Exception
         */
        public function giveMoney(MoneyOperator $userRecipient, float $paymentAmount): bool
        {
            if (!$this->moneyController->giveMoneyTransactionFullCheck($this, $userRecipient, $paymentAmount)) {
                echo "giveMoney transaction failed"; //TODO all msg in MsgData
                return false;
            }
            $reservedUserBalance = $this->getBalance();
            $reservedUserRecipientBalance = $userRecipient->getBalance();
            try {
                $this->setBalance($this->getBalance() - $paymentAmount);
                $userRecipient->setBalance($userRecipient->getBalance() + $paymentAmount);
                echo "\nUser <{$this->getName()}> transferred <{$paymentAmount}> money to user <{$userRecipient->getName()}>”\n";
                return true;
            } catch (\Exception $ex) {
                $this->setBalance($reservedUserBalance);
                $userRecipient->setBalance($reservedUserRecipientBalance);
                echo "transaction error occurred.";
                throw $ex;
            }

        }

        protected function setBalance(float $balance): bool
        {
            // TODO check & control needed here or in abstract parent!?
            return parent::setBalance($balance); // TODO: Change the autogenerated stub
        }

        /**
         * TODO was forced to weaken the access modifier for the money controller
         * @return float
         */
        public function getBalance(): float
        {
            return parent::getBalance(); // TODO: Change the autogenerated stub
        }


        /**
         * @return bool
         */
        public function printStatus(): bool
        {
            // TODO: Implement printStatus() method.
            echo "\n"."User \"{$this->getName()}\" has \"{$this->getBalance()}\" money on balance.\n";
            return true;
        }

        /**
         * TODO maybe redundant
         * @param MoneyOperator $fromUser
         * @param float $amountOfMoney
         * @return bool
         */
        public function takeMoney(MoneyOperator $fromUser, float $amountOfMoney): bool
        {
            // TODO: Implement takeMoney() method.
            return true;
        }

        /**
         * @return MoneyController|null
         */
        public function getMoneyController(): ?MoneyController
        {
            return $this->moneyController;
        }

        /**
         * TODO all msgs need move to Msg class. not yet.
         */
        private function notifyCreation(): void
        {
            echo "\n"."User \"{$this->getName()}\" created successful!\nHas \"{$this->getBalance()}\" money on balance.\n";
        }


    }

    // TODO Round when money transaction
    // TODO msg in MsgData