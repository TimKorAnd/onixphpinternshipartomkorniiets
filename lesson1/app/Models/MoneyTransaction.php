<?php

    namespace app\Models;

    use app\Models;

    /**
     * Interface MoneyTransaction
     * @package app\Models
     */
    interface MoneyTransaction
    {
        function giveMoney(Models\MoneyOperator $userRecipient, float $amountOfPaymentAmount): bool;

        function takeMoney(Models\MoneyOperator $fromUser, float $amountOfMoney): bool;

        function printStatus(): bool;//TODO where? return?
    }