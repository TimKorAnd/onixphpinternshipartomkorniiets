<?php

    namespace app\Models;

    use app\Exception;

    abstract class MoneyOperator
    {
        private string $name;
        private float $balance;
        private bool $isBank;

        /**
         * MoneyOperator constructor.
         * @param string $name
         * @param float $balance
         * @param bool $isBank
         */
        public function __construct(string $name, float $balance, bool $isBank = false)
        {
            $this->name = $name;
            $this->balance = $balance;
            $this->isBank = $isBank;
            //echo get_class() . ' abstractClass constructor run' . "\n";
        }


        /**
         * @return string
         */
        protected function getName(): string
        {
            return $this->name;
        }

        /**
         * @return float
         */
        protected function getBalance(): float
        {
            return $this->balance;
        }

        /**
         * @param float $balance
         * @return bool
         */
        protected function setBalance(float $balance): bool
        {
            // TODO check & control needed here or in heirs!?
            $this->balance = $balance;
            return true;
        }

        /**
         * @return bool
         */
        public function isBank(): bool
        {
            return $this->isBank;
        }
    }