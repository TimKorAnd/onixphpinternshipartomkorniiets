<?php

    namespace app\Controllers;

    use app\Models\Bank;
    use app\Models\MoneyOperator;
    use app\MoneyExceptions\MoneyException;
    use app\Models\User;


    //require_once('../MoneyExceptions/MoneyException.php');


    /**
     * Class MoneyController
     * Logic for money & transaction control.
     * @package app\Controllers
     */
    class MoneyController
    {
        private float $initBalanceMin;
        private float $initBalanceMax;
        private float $balanceMin;
        private float $balanceMax;
        private float $paymentMin;
        private float $paymentMax;
        private bool $bankMonopoly; //if true then positive balance can't be created, monet may take from bank only
        private float $initBalanceBankAllowed;
        //TODO transaction limits ? Float math round($f, 2) s
        //TODO name check && log

        /**
         * MoneyController constructor.
         * @param bool $bankMonopoly - true prohibits creating users with a balance bigger than banks allowed if a bank exists
         * perhaps this parameter should be stored in the bank
         */
        public function __construct(bool $bankMonopoly = false)
        {
            $this->bankMonopoly = $bankMonopoly;
            $this->initBalanceMin = 0;
            $this->initBalanceMax = PHP_FLOAT_MAX;
            $this->balanceMin = -PHP_FLOAT_MAX;
            $this->balanceMax = PHP_FLOAT_MAX;
            $this->paymentMin = 0;
            $this->paymentMax = PHP_FLOAT_MAX;
            $this->initBalanceBankAllowed = 0; //setters for bellow expected

        }

        /*public function __construct(float ...$params)
        {
            ['balanceMin' => $this->balanceMin, 'balanceMax' => $this->balanceMax,
              'paymentMin' => $this->paymentMin, 'paymentMax' => $this->paymentMax,
              'initBalanceMin' => $this->initBalanceMin, 'initBalanceMax' => $this->initBalanceMax] = $params;
        }*/ //TODO how to store same default values when destructing assigning have been go on? If miss any of
//TODO             key names exception arise ;(

        public function giveMoneyTransactionFullCheck(MoneyOperator $userPayer, MoneyOperator $userRecipient, float $paymentAmount): bool
        {
            if (!($userPayer instanceof User && $userRecipient instanceof User )){
                echo "Only for user participation"; //TODO all msg in MsgData
                return false;
            }
            if ($userPayer === $userRecipient){
                echo "it is impossible to perform an operation with the same user"; //TODO all msg in MsgData
                return false;
            }
            //TODO changed type in params to User from MoneyOperator, else called protected getBallance from there
            if (!$this->paymentCheck($paymentAmount)) {
                echo "bad payment amount"; //TODO all msg in MsgData
                return false;
            }
            if (!$this->balanceCheck($userPayer->getBalance() - $paymentAmount)) { // getBalance made public, its bad?
                echo "Payer bad balance after payment"; //TODO all msg in MsgData
                return false;
            };
            if (!$userRecipient->getMoneyController()->balanceCheck($userRecipient->getBalance() + $paymentAmount)) {
                echo "Recipient bad balance after payment"; //TODO all msg in MsgData
                return false;
            };
            return true;
        }

        public function balanceCheck(float $balance): bool
        {
            return ($balance >= $this->balanceMin && $balance <= $this->balanceMax);
        }

        public function paymentCheck(float $payment): bool
        {
            return ($payment >= $this->paymentMin && $payment <= $this->paymentMax);
        }

        public function initBalanceCheck(float $initBalance): bool
        {
            if (!($initBalance >= $this->initBalanceMin && $initBalance <= $this->initBalanceMax)) {
                throw new MoneyException("Initial balance {$initBalance} is out of limits:\nfrom min = {$this->initBalanceMin} to max = {$this->initBalanceMax}");
            }
            return true;
        }

        public function getInitBalanceBankAllowed(float $initBalance, string $name): float
        {
            if ($this->bankMonopoly && Bank::isBankExist()) {
                if (round($initBalance, 2) > round($this->initBalanceBankAllowed, 2)) {
                    $bankName = Bank::getBankName();
                    //TODO all echo msg in constants.
                    echo "\n!!! New created user \"{$name}\" can't take balance \"{$initBalance}\" now,
                    because the \"${bankName}\" bank already exist.\nContact the \"${bankName}\" to receive money.\n";
                    $initBalance = $this->initBalanceBankAllowed;
                }
            }
            return $initBalance;
        }

        /**
         * @param float|int $initBalanceMin
         */
        public function setInitBalanceMin($initBalanceMin): MoneyController
        {
            $this->initBalanceMin = $initBalanceMin;
            return $this;
        }

        /**
         * @param float $initBalanceMax
         */
        public function setInitBalanceMax(float $initBalanceMax): MoneyController
        {
            $this->initBalanceMax = $initBalanceMax;
            return $this;
        }

        /**
         * @param float|int $balanceMin
         */
        public function setBalanceMin($balanceMin): MoneyController
        {
            $this->balanceMin = $balanceMin;
            return $this;
        }

        /**
         * @param float $balanceMax
         */
        public function setBalanceMax(float $balanceMax): MoneyController
        {
            $this->balanceMax = $balanceMax;
            return $this;
        }

        /**
         * @param float|int $paymentMin
         */
        public function setPaymentMin($paymentMin): MoneyController
        {
            $this->paymentMin = $paymentMin;
            return $this;
        }

        /**
         * @param float|int $initBalanceBankAllowed
         */
        public function setInitBalanceBankAllowed($initBalanceBankAllowed): MoneyController
        {
            $this->initBalanceBankAllowed = $initBalanceBankAllowed;
            return $this;
        }

        /**
         * @param float $paymentMax
         */
        public function setPaymentMax(float $paymentMax): MoneyController
        {
            $this->paymentMax = $paymentMax;
            return $this;
        }

    }